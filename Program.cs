﻿using NUnit.Framework;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

class googleSearch {

    IWebDriver driver;
    WebDriverWait wait;

    [SetUp]
    public void start() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
    }
    
    [Test]
    public void testGoogleSearch() {
        driver.Navigate().GoToUrl("https://www.google.com/ncr");
        driver.FindElement(By.XPath("//button[normalize-space()='I agree']")).Click();
        driver.FindElement(By.Name("q")).SendKeys("cheese" + Keys.Enter);
        wait.Until(webDriver => webDriver.FindElement(By.CssSelector("h3")).Displayed);
        IWebElement firstResult = driver.FindElement(By.CssSelector("h3"));
        Console.WriteLine(firstResult.GetAttribute("textContent"));
    }

    [TearDown]
    public void stop() {
        driver.Close();
    }
}